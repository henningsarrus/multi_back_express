import 'package:flutter/material.dart';

import './pages/startseite.dart';

void main() {
  runApp(new MaterialApp(
    home: new Startseite(),
  ));
  theme: ThemeData(
    canvasColor: Colors.blueGrey,
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    accentColor: Colors.green,
    brightness: Brightness.dark,
  );
}