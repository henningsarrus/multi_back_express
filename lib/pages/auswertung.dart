import 'package:flutter/material.dart';
import 'package:tts/tts.dart';
import './startseite.dart';


class Auswertung extends StatelessWidget {

  final int benoetigteAntworZeit;
  final int quote;
  int erreichteLevel;

  Auswertung(this.benoetigteAntworZeit, this.quote, this.erreichteLevel);

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.blueAccent,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          benoetigteAntworZeit > 0 ? new Text("Du hast durchschnittlich: ", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),) : new Text(""),
          benoetigteAntworZeit > 0 ? new Text(benoetigteAntworZeit.toString()+ " ms", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 40.0)) : new Text(""),
          benoetigteAntworZeit > 0 ? new Text("für eine richtige Antwort gebraucht.", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),) : new Text(""),
          new Text(quote.toString() + " Prozent", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 40.0),),
          new Text("deiner Antworten waren richtig.", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),),
          new Text("Level " + erreichteLevel.toString(), style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 40.0),),
          new Text("hast du erreicht.", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),),
          new IconButton(
            icon: new Icon(Icons.arrow_right),
            color: Colors.white,
            iconSize: 50.0,
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new Startseite()), (Route route) => route == null)
          )
        ],
      )
    );
  }
}