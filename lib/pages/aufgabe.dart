import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:tts/tts.dart';
import 'package:vibration/vibration.dart';
import 'package:audioplayers/audio_cache.dart';
import 'dart:async';
import './startseite.dart';
import './auswertung.dart';
import 'package:flutter_test/flutter_test.dart';

const falsche_antwort = "falsche_antwort.wav";
const naechste_level = "naechste_level.wav";
const richtige_antwort_2 = "richtige_antwort_2.mp3";
const training_gestartet = "training_gestartet.wav";
const vorherige_level = "vorherige_level.wav";


const milliSecond = const Duration(milliseconds:1);

class Spiel extends StatefulWidget {
  @override
  State createState() => new SpielState();
}

class SpielState extends State<Spiel> with TickerProviderStateMixin, WidgetsBindingObserver {
  //Audioauswertung
  static AudioCache player = new AudioCache();
  Timer counterSeconds;

  //IconGroesse
  double aktuelleIconGroesse = 100;

  AnimationController controller;
  bool SpielGestartet = false;

  //Variablen / Klassen für Snackbar
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String snackbarText;
  Color snackbarFarbe = Colors.grey;

  //Icon auswerten
  List<IconAuswerten> iconStatus = [IconAuswerten(1, 1, 100), IconAuswerten(1, 1, 100), IconAuswerten(1, 1, 100), IconAuswerten(1, 1, 100)];

  //akustische Aufgabe
  AkustischeAusgabe akustischeStatus = new AkustischeAusgabe();

  //Spielklasse
  SpielVariablen spielZustand = new SpielVariablen();

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    spielZustand.level = 25; //Default Level erhöht
    aktuelleIconGroesse = 150;
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 12),
    );
    spielGestartet();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    spielBeendet(true);
  }

  Future<bool> _willPopCallback() {
    spielBeendet(true);
  }

  void spielGestartet(){
    print("Spiel gestartet");
    controller.addStatusListener((status) {
      if(SpielGestartet && status == AnimationStatus.dismissed && controller.value == 0.0) {
        zeitAbgelaufen();
      }
    });
    spielZustand.punkte = 0;
    spielZustand.trainingsZeit = 240;

    Timer.periodic(Duration(seconds: 1), (timer) {
      trainingBeendenNachZeit();
    });

    SpielGestartet = true;
    player.play(training_gestartet);
    levelVerwalten();
  }

  void trainingBeendenNachZeit(){
      setState((){
        spielZustand.trainingsZeit--;
      });

    if(spielZustand.trainingsZeit == 0){
      spielBeendet(false);
    }
  }


  void spielBeendet(bool abbruch){
    if(SpielGestartet){

      SpielGestartet = false;
      controller.reset();
      controller.stop();

      if(!abbruch){
        int benoetigteZeit = (spielZustand.benoetigteZeitenSumme / (spielZustand.korrekteAntwortenGesamt == 0 ? 1 : spielZustand.korrekteAntwortenGesamt)).round();
        benoetigteZeit < 100 ?  Tts.speak("Das Training wurde beendet.") :
        Tts.speak("Du hast durchschnittlich "
            + benoetigteZeit.toString()
            + "Millisekunden für eine richtige Antwort benötigt.");

        Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new Auswertung( (spielZustand.benoetigteZeitenSumme / (spielZustand.korrekteAntwortenGesamt == 0 ? 1 : spielZustand.korrekteAntwortenGesamt) ).round(), ((spielZustand.korrekteAntwortenGesamt / (spielZustand.fragenGesamt == 0 ? 1 : spielZustand.fragenGesamt) ) * 100).round(), spielZustand.level)), (Route route) => route == null);

      }else{
        Tts.speak("Das Training wurde abgebrochen.");
        Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new Startseite()), (Route route) => route == null);
      }
    }
  }

  void richtigGedrueckt(){
    if(SpielGestartet){

      bool antwortIstRichtig = false;

      if(spielZustand.level > 10 && spielZustand.level < 21){
        if(akustischeStatus.gleichheitPruefen() || iconStatus[0].gleichheitPruefen() || iconStatus[1].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }else if(spielZustand.level > 20 && spielZustand.level < 31){
        if(akustischeStatus.gleichheitPruefen() || iconStatus[0].gleichheitPruefen() || iconStatus[1].gleichheitPruefen() || iconStatus[2].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }else if(spielZustand.level > 30){
        if(akustischeStatus.gleichheitPruefen() || iconStatus[0].gleichheitPruefen() || iconStatus[1].gleichheitPruefen() || iconStatus[2].gleichheitPruefen() || iconStatus[3].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }else{
        if(akustischeStatus.gleichheitPruefen() || iconStatus[0].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }

      if(antwortIstRichtig){
        spielZustand.punkte += spielZustand.level;
        spielZustand.korrekteAntwortenInFolge++;
        spielZustand.korrekteAntwortenGesamt++;
        spielZustand.falscheAntwortenInFolge = 0;
        spielZustand.korrekteAntwortenInFolge == 5 && spielZustand.level < 40 ? player.play(naechste_level) : player.play(richtige_antwort_2);
        counterSeconds.cancel();
        spielZustand.benoetigteZeitenSumme += counterSeconds.tick;
      }
      else{
        spielZustand.falscheAntwortenInFolge++;
        spielZustand.korrekteAntwortenInFolge = 0;
        Vibration.vibrate(duration: 200);
        spielZustand.falscheAntwortenInFolge == 2 && spielZustand.level > 1 ? player.play(vorherige_level) : player.play(falsche_antwort);
        counterSeconds.cancel();
        print(counterSeconds.tick);
      }
      levelVerwalten();
    }
  }

  void falschGedrueckt(){
    if(SpielGestartet){

      bool antwortIstRichtig = false;

      if(spielZustand.level > 10 && spielZustand.level < 21){
        if(!akustischeStatus.gleichheitPruefen() && !iconStatus[0].gleichheitPruefen() && !iconStatus[1].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }else if(spielZustand.level > 20 && spielZustand.level < 31){
        if(!akustischeStatus.gleichheitPruefen() && !iconStatus[0].gleichheitPruefen() && !iconStatus[1].gleichheitPruefen() && !iconStatus[2].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }
      else if(spielZustand.level > 30){
        if(!akustischeStatus.gleichheitPruefen() && !iconStatus[0].gleichheitPruefen() && !iconStatus[1].gleichheitPruefen() && !iconStatus[2].gleichheitPruefen() && !iconStatus[3].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }else{
        if(!akustischeStatus.gleichheitPruefen() && !iconStatus[0].gleichheitPruefen()){
          antwortIstRichtig = true;
        }
      }

      if(antwortIstRichtig){
        spielZustand.punkte += (5 * spielZustand.level);
        spielZustand.korrekteAntwortenInFolge++;
        spielZustand.korrekteAntwortenGesamt++;
        spielZustand.falscheAntwortenInFolge = 0;
        spielZustand.korrekteAntwortenInFolge == 5 && spielZustand.level < 40 ? player.play(naechste_level) : player.play(richtige_antwort_2);
        counterSeconds.cancel();
        spielZustand.benoetigteZeitenSumme += counterSeconds.tick;
      }
      else{
        spielZustand.falscheAntwortenInFolge++;
        spielZustand.korrekteAntwortenInFolge = 0;
        Vibration.vibrate(duration: 200);
        spielZustand.falscheAntwortenInFolge == 2 && spielZustand.level > 1 ? player.play(vorherige_level) : player.play(falsche_antwort);
        counterSeconds.cancel();
        print(counterSeconds.tick);
      }
      levelVerwalten();
    }
  }

  void zeitAbgelaufen(){
    print("Zeit abgelaufen");
    spielZustand.falscheAntwortenInFolge++;
    spielZustand.korrekteAntwortenInFolge = 0;
    Vibration.vibrate(duration: 200);
    spielZustand.falscheAntwortenInFolge == 2 && spielZustand.level > 1 ? player.play(vorherige_level) : player.play(falsche_antwort);
    counterSeconds.cancel();
    print(counterSeconds.tick);

    levelVerwalten();
  }

  void neueAufgabeStellen(){
    counterSeconds = new Timer.periodic(milliSecond, (Timer t) => {});
    controller.reverse(from: 1.0);

    /*
    * nach Binominalverteilung:
    * fünf Symbole und Zahlen, wenn ein Symbol gezeigt wird
    * acht Symbole und Zahlen, wenn zwei Symbole gezeigt werden
    * */
    int anzahlSymbole = 3;

    spielZustand.level < 11 ? anzahlSymbole = 5 :
    spielZustand.level > 10 && spielZustand.level < 21 ? anzahlSymbole = 8 :
    spielZustand.level > 20 && spielZustand.level < 31 ? anzahlSymbole = 11 : anzahlSymbole = 13;

    //neue Aufgabe für Icon festlegen
    for(int i=0; i < 4; i++){
      iconStatus[i].symbolUndFarbeFestlegen(anzahlSymbole, 10);
    }

    //neue Aufgabe für Sprache festlegen
    akustischeStatus.neueZahlFestlegen(anzahlSymbole);

    //Sprachausgabe starten
    akustischeStatus.sprachausgabe();

    spielZustand.fragenGesamt++;
  }

  void levelVerwalten(){
    if(spielZustand.korrekteAntwortenInFolge > 4 && spielZustand.level < 40){
      spielZustand.korrekteAntwortenInFolge = 0;
      spielZustand.level++;
      snackbarText = "nächste Level erreicht";
      snackbarFarbe = Colors.green;
      _snackbarAnzeigen;
    }
    else if(spielZustand.korrekteAntwortenInFolge > 4){
      spielZustand.korrekteAntwortenInFolge = 5;
    }
    if(spielZustand.falscheAntwortenInFolge > 1 && spielZustand.level > 1){
      spielZustand.falscheAntwortenInFolge = 0;
      spielZustand.level--;
      snackbarText = "Level verringert";
      snackbarFarbe = Colors.orangeAccent;
      _snackbarAnzeigen;
    }
    else if(spielZustand.falscheAntwortenInFolge > 1){
      spielZustand.falscheAntwortenInFolge = 2;
    }

    spielZustand.level > 30 ? aktuelleIconGroesse = 25.0 :
        spielZustand.level > 20 && spielZustand.level < 31 ? aktuelleIconGroesse = 50.0 :
        spielZustand.level > 10 && spielZustand.level < 21 ? aktuelleIconGroesse = 75.0 : aktuelleIconGroesse = 150.0;


    int aktuelleZeitdruck = (spielZustand.level - 1) % 10;
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 12 - aktuelleZeitdruck),
    );


    controller.addStatusListener((status) {
      if(SpielGestartet && status == AnimationStatus.dismissed && controller.value == 0.0) {
        zeitAbgelaufen();
      }
    });
    setState((){
      neueAufgabeStellen();
    });
  }

  // Display Snackbar
  void get _snackbarAnzeigen {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: Duration(milliseconds: 1500),
      content: Text(snackbarText),
      backgroundColor: snackbarFarbe,
    ));
  }


  @override
  Widget build(BuildContext context) {
    //ThemeData themeData = Theme.of(context);
    return new WillPopScope(
        child: new Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.blueGrey,
          body: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.center,
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: AnimatedBuilder(
                              animation: controller,
                              builder: (BuildContext context, Widget child) {
                                return new CustomPaint(
                                    painter: TimerPainter(
                                      animation: controller,
                                      backgroundColor: Colors.white,
                                      color: Colors.red,
                                    ));
                              },
                            ),
                          ),

                          Align(
                            alignment: FractionalOffset.center,
                            child:
                            spielZustand.level > 30 ?
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconAuswerten(iconStatus[0].aktuelleIcon, iconStatus[0].aktuelleFarbe, aktuelleIconGroesse),
                                  IconAuswerten(iconStatus[1].aktuelleIcon, iconStatus[1].aktuelleFarbe, aktuelleIconGroesse ),
                                  IconAuswerten(iconStatus[2].aktuelleIcon, iconStatus[2].aktuelleFarbe, aktuelleIconGroesse),
                                  IconAuswerten(iconStatus[3].aktuelleIcon, iconStatus[3].aktuelleFarbe, aktuelleIconGroesse),
                                ],
                              ) :
                            spielZustand.level > 20 && spielZustand.level < 31 ?
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconAuswerten(iconStatus[0].aktuelleIcon, iconStatus[0].aktuelleFarbe, aktuelleIconGroesse),
                                  IconAuswerten(iconStatus[1].aktuelleIcon, iconStatus[1].aktuelleFarbe, aktuelleIconGroesse ),
                                  IconAuswerten(iconStatus[2].aktuelleIcon, iconStatus[2].aktuelleFarbe, aktuelleIconGroesse),
                                ],
                              ) :
                            spielZustand.level > 10 &&  spielZustand.level < 21 ?
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconAuswerten(iconStatus[0].aktuelleIcon, iconStatus[0].aktuelleFarbe, aktuelleIconGroesse),
                                  IconAuswerten(iconStatus[1].aktuelleIcon, iconStatus[1].aktuelleFarbe, aktuelleIconGroesse ),
                                ],
                              ) :
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconAuswerten(iconStatus[0].aktuelleIcon, iconStatus[0].aktuelleFarbe, aktuelleIconGroesse),
                                ],
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new RaisedButton(
                          child: new Text("Richtig", style: new TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontSize: 40.0)),
                          color: Colors.green,
                          onPressed:() {
                            setState((){
                              richtigGedrueckt();
                            });
                          }
                      ),
                      new RaisedButton(
                          child: new Text("Falsch", style: new TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontSize: 40.0)),
                          color: Colors.red,
                          onPressed:() {
                            setState((){
                              falschGedrueckt();
                            });
                          }
                      ),
                    ],
                  ),
                ),



                Container(
                  //margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FloatingActionButton(
                        child: AnimatedBuilder(
                          animation: controller,
                          builder: (BuildContext context, Widget child) {
                            return new Icon(Icons.stop);
                          },
                        ),
                        onPressed: () {
                          print("Spielbutton gedrückt");
                          spielBeendet(true);
                        },
                      ),
                    ],
                  ),
                ),


                Container(
                  //margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Text("Punkte: " + spielZustand.punkte.toString(), style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                      new Text("Level: " + spielZustand.level.toString() + " / 40", style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                    ],
                  ),
                ),
                Container(
                  //margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Text("Aufsteig: " + spielZustand.korrekteAntwortenInFolge.toString() + " / 5", style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                      new Text("Abstieg: " + spielZustand.falscheAntwortenInFolge.toString() + " / 2", style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                    ],
                  ),
                ),
                Container(
                  //margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Text("Antwortzeit: " + (spielZustand.benoetigteZeitenSumme / (spielZustand.korrekteAntwortenGesamt == 0 ? 1 : spielZustand.korrekteAntwortenGesamt)).round().toString() + " ms", style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                    ],
                  ),
                ),

                Container(
                  //margin: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Text("verbleibende Zeit: " + spielZustand.trainingsZeit.toString() + " Sekunden", style: new TextStyle(color: Colors.white, fontSize: 20.0)),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
        onWillPop: _willPopCallback
    );
  }
}

class TimerPainter extends CustomPainter {
  TimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 5.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}

class AkustischeAusgabe {
  //Varbiablen für aktuelle Positionen
  int aktuelleAusgabe;
  int vorherigeAusgabe;

  //aktustische Ausgabe
  List<int> akustischeZahl = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

  //neue Symbol und Farbe festlegen
  void neueZahlFestlegen(int anzahlZahlen){
    vorherigeAusgabe = aktuelleAusgabe;

    var rng = new math.Random();
    aktuelleAusgabe = rng.nextInt(anzahlZahlen);

  }

  //Auswerten, ob Farbe oder Symbol mit vorher gezeigtem übereinstimmt
  bool gleichheitPruefen(){
    return (aktuelleAusgabe == vorherigeAusgabe);
  }

  sprachausgabe() async {
    Tts.speak(akustischeZahl[aktuelleAusgabe].toString());
  }
}

class IconAuswerten extends StatelessWidget{
  //Icon Größe
  double groesseIcon = 100.0;

  //Icons zum Anzeigen
  List<IconData> iconArten = [Icons.ac_unit, Icons.announcement, Icons.access_alarm, Icons.accessibility, Icons.accessible, Icons.account_balance, Icons.account_balance_wallet, Icons.account_circle, Icons.add_alert, Icons.airline_seat_individual_suite, Icons.bluetooth_audio, Icons.border_color, Icons.branding_watermark, Icons.cake, Icons.child_friendly];
  List<Color> farbArten = [Colors.amber, Colors.green, Colors.greenAccent, Colors.purpleAccent, Colors.pink, Colors.black, Colors.grey, Colors.white, Colors.red, Colors.brown, Colors.cyan,  Colors.orange,  Colors.yellow];

  //Varbiablen für aktuelle Positionen
  int aktuelleIcon;
  int aktuelleFarbe;

  IconAuswerten(this.aktuelleIcon, this.aktuelleFarbe, this.groesseIcon);

  //Variablen für vorherige Positionen
  int _vorherigeIcon;
  int _vorherigeFarbe;

  //neue Symbol und Farbe festlegen
  void symbolUndFarbeFestlegen(int anzahlSymbole, int anzahlFarben){
    _vorherigeIcon = aktuelleIcon;
    _vorherigeFarbe = aktuelleFarbe;

    var rng = new math.Random();
    aktuelleIcon = rng.nextInt(anzahlSymbole);

    //Farbe festlegen
    aktuelleFarbe = rng.nextInt(anzahlFarben);
  }

  //Auswerten, ob Farbe oder Symbol mit vorher gezeigtem übereinstimmt
  bool gleichheitPruefen(){
    return (_vorherigeIcon == aktuelleIcon || _vorherigeFarbe == aktuelleFarbe);
  }

  @override
  Widget build(BuildContext context) {
    return new Icon(iconArten[aktuelleIcon], size: groesseIcon, color: farbArten[aktuelleFarbe]);
  }
}

class SpielVariablen {
  //Variablen
  int punkte = 0, korrekteAntwortenInFolge = 0, falscheAntwortenInFolge = 0, level = 0;
  int korrekteAntwortenGesamt = 0, fragenGesamt = 0;
  double benoetigteZeitenSumme = 0;
  int trainingsZeit = 240;
}